// RESOURCES
var genO = function(label, values, more) {
  return {
    label: label,
    values: values || [],
    more: more || ''
  }
}

var questions = [{
  q: 'What\'s your age group?',
  sq: 'Age can have an impact on your nails.',
  o: [genO('19 & UNDER'), genO('20-35'), genO('36-50'), genO('51-65'), genO('66+')],
  a: null,
  key: 'age'
}, {
  q: 'What type of product are you looking for?',
  o: [
      genO('TOP COATS', ['topCoat']),
      genO('BASE COATS', ['baseCoat']),
      genO('GROWTH', ['growth']),
      genO('STRENGTHENERS', ['strengthener']),
      genO('THICKENERS', ['thickener']),
      genO('RIDGE FILLERS', ['ridgeFiller']),
      genO('NAIL OILS', ['nailOil']),
      genO('MULTI-BENEFIT PRODUCTS', ['multiBenefit']),
      genO('REPAIR FOR DAMAGED NAILS', ['repair'])
  ],
  value: 2,
  a: null,
  key: 'product_type'
}, {
  q: 'You would like your nails to be:',
  o: [
    genO('THICKER', ['thickener']),
    genO('SMOOTHER', ['ridgeFiller']),
    genO('LONGER', ['growth']),
    genO('STRONGER', ['strengthener']),
    genO('SHINIER', ['topCoat', 'shineGloss']),
    genO('POLISH TO LAST LONGER', ['topCoat', 'baseCoat']),
  ],
  value: 1,
  a: null,
  key: 'would_like'
}, {
  q: 'How thick are your nails?',
  o: [genO('THIN', null, 'Nails that bend easily'), genO('NORMAL', null, 'Nails that don\'t bend easily and break occasionally'), genO('THICK', null, 'Nails that are brittle and crack easily')],
  a: null,
  key: 'thick'
}, {
  q: 'What is your nail type?',
  o: [
    genO('SOFT, WEAK, PEELING IN LAYERS', ['thickener']),
    genO('DRY, BRITTLE, CRACKING', ['ridgeFiller', 'strengthener']),
    genO('SPLIT, CHIPPED, BREAKING', ['growth', 'strengthener']),
    genO('DON\'T GROW', ['growth']),
    genO('HAVE RIDGES OR SMALL GROOVES', ['ridgeFiller', 'topCoat'])
  ],
  value: 1,
  a: null,
  key: 'nail_type'
}, {
  q: 'Currently, you have:',
  o: [genO('GEL NAILS'), genO('ACRYLIC NAILS'), genO('DIP NAILS'), genO('GLUE-ON NAILS'), genO('NATURAL NAILS')],
  a: null,
  key: 'currently'
}, {
  q: 'Which of these best applies to you?',
  o: [genO('USING NAIL POLISH REMOVER MORE THAN ONCE A WEEK'), genO('WEARING GEL OR DIP MANICURES'),
    genO('CLEANING AND WASHING DISHES WITHOUT GLOVES'), genO('BUFFING YOUR NAILS'), genO('USING HAND SANITIZER OFTEN')
  ],
  a: null,
  key: 'best'
}, {
  q: 'In which state do you live?',
  sq: 'The climate of the region where you reside can affect the structure of your nails.',
  o: 'state-list',
  a: null,
  key: 'state'
}]
// console.log(questions)

var products = allProducts.filter(e => {
  return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23].indexOf(e.id) !== -1
})

var learnMore = function(url){
  return '<a href="' + url + '" target="_blank" ><div class="learnmore">Learn more</div></a>';
}