// LOGIC
var currentStep = 0;
var answers = {};

var attributes = {
  topCoat : 0,
  baseCoat : 0,
  strengthener : 0,
  growth : 0,
  thickener : 0,
  ridgeFiller : 0,
  nailOil : 0,
  multiBenefit : 0,
  repair : 0,
  shineGloss : 0,
}
var resetAttributes = function () {
  attributes = {
    topCoat : 0,
    baseCoat : 0,
    strengthener : 0,
    growth : 0,
    thickener : 0,
    ridgeFiller : 0,
    nailOil : 0,
    multiBenefit : 0,
    repair : 0,
    shineGloss : 0,
  }
}

var attributesDesc = {
  topCoat : 'Top coat',
  baseCoat : 'Can be used as a base coat',
  strengthener : 'Helps to strengthen weak nails',
  growth : 'Encourages nail growth',
  thickener : 'Helps to thicken thin nails',
  ridgeFiller : 'Fills in ridges for a smooth look',
  nailOil : 'This product is a nail oil',
  multiBenefit : 'Includes multiple benefits such as strengthening, growth, base and top coat, and more.',
  repair : 'Helps repair nails that have been damaged by acrylics or gels',
  shineGloss : 'Provides ultra shine and gloss',
}


for(var i=0; i<products.length; i++) {
  var src = products[i].img;
  var $img = $('<img>');
  $img.attr('src', src);
  //$('body').append($img);
}

var nextButton = function() {
  return '<div class="nextbtn"><img src="'+window.quizServer+'/img/nextarrow.png"></div>';
}
var quizAgain = function() {
  return '<div class="quizagain"><div class="quizcircle"><img src="'+window.quizServer+'/img/nextarrow.png"></div>Take quiz again</div>'
}

var getTick = function(tick) {
  return  '<img class="aspect-value" src="'+ window.quizServer + '/img/' + tick + '.png">';
}

var dropdownMenu = function() {
  return '<div class="dropdown-choose-state">'
          + '<select>'
          +  '<option>Choose your state</option>'
          +  '<option>Alabama</option>'
          +  '<option>Alaska</option>'
          +  '<option>Arizona</option>'
          +  '<option>Arkansas</option>'
          +  '<option>California</option>'
          +  '<option>Colorado</option>'
          +  '<option>Connecticut</option>'
          +  '<option>Delaware</option>'
          +  '<option>Florida</option>'
          +  '<option>Georgia</option>'
          +  '<option>Hawaii</option>'
          +  '<option>Idaho</option>'
          +  '<option>Illinois</option>'
          +  '<option>Indiana</option>'
          +  '<option>Iowa</option>'
          +  '<option>Kansas</option>'
          +  '<option>Kentucky</option>'
          +  '<option>Louisiana</option>'
          +  '<option>Maine</option>'
          +  '<option>Maryland</option>'
          +  '<option>Massachusetts</option>'
          +  '<option>Michigan</option>'
          +  '<option>Minnesota</option>'
          +  '<option>Mississippi</option>'
          +  '<option>Missouri</option>'
          +  '<option>Montana</option>'
          +  '<option>Nebraska</option>'
          +  '<option>Nevada</option>'
          +  '<option>New Hampshire</option>'
          +  '<option>New Jersey</option>'
          +  '<option>New Mexico</option>'
          +  '<option>New York</option>'
          +  '<option>North Carolina</option>'
          +  '<option>North Dakota</option>'
          +  '<option>Ohio</option>'
          +  '<option>Oklahoma</option>'
          +  '<option>Oregon</option>'
          +  '<option>Pennsylvania</option>'
          +  '<option>Rhode Island</option>'
          +  '<option>South Carolina</option>'
          +  '<option>South Dakota</option>'
          +  '<option>Tennessee</option>'
          +  '<option>Texas</option>'
          +  '<option>Utah</option>'
          +  '<option>Vermont</option>'
          +  '<option>Virginia</option>'
          +  '<option>Washington</option>'
          +  '<option>West Virginia</option>'
          +  '<option>Wisconsin</option>'
          +  '<option>Wyoming</option>'
          + '</select>'
        + '</div>';
}




$(".quiz-content").append('<div class="step">')

// SHOW QUESTIONS
function showQuestion(ndx) {
  var question = questions[ndx];
  var $step = $('.step');
  $step.empty();
  $step.append( '<div class="question">' + question.q + "</div>" );

  if (question.sq) {
    $step.append( '<div class="sub-question">' + question.sq + "</div>" );
  }

  if (question.o === 'state-list') {
    var $stateDropdown = $(dropdownMenu());
    $stateDropdown.on('change', onQuizStateChange)
    $step.append($stateDropdown);
  } else {
    for (i = 0; i < question.o.length; i++) {
      var option = question.o[i];
      var $option = $('<div class="answer" value="' + option.values.join(',') +'" ><div class="main">' + option.label + '</div></div>');
      if (option.more) {
        $option.append('<div class="more">'+option.more+'</div>')
      }
      $step.append($option);
    }
  }

}


showQuestion(currentStep);




function answerQuestion(e) {

  var $answer = $(this);
  var question = questions[currentStep];
  answers[question.key] = $answer.text();
  var value = $answer.attr('value').split(',')
  for (i = 0; i < value.length; i++) {
    var att = value[i]
    attributes[att] += (question.value ? question.value : 1);
  }

  currentStep++;
  if(questions.length > currentStep) {
    showQuestion(currentStep);
  } else {
    $( ".step" ).empty();
    showStatesQuestion();
    result();
  }
}
// ANSWER CLICK
$( document ).on('click', ".answer", answerQuestion);


function showbtn() {
  var $button = $('.step .nextbtn');
  if (!$button.length) $(".step").append(nextButton());
}

// ONCHANGE DROPDOWN
function onQuizStateChange(evt) {
  answers.state = $('option:selected', $(this)).text();
  showbtn();
}

// END QUIZ CLICK, FUNCTION
var showMore = function() {
  return '<div class="show-more">Show more</div>';
}
var showLess = function() {
  return '<div class="show-less">Show less</div>';
}

function buildProduct (product) {
  $finalProduct = $('<div class="finalproduct"></div>');
  $name = $('<div class="productname">' + product.name + '</div>');
  $img = $('<img width="100%">');
  $img.attr('src', product.img);
  $space = $('<div class="whitespace"></div>');
  $(".step").append($finalProduct);
  $description = $('<ul class="description"></ul>')
  $finalProduct.append($name, $img, $space, $description);
  $finalProduct.append($name, $img, $space);
  aspects(product);
  //$finalProduct.append(showMore);
  if (learnMore) {
    $finalProduct.append(learnMore(product.url))
  }
}

var aspects = function(element) {
  for (var i = 0; i < element.attributes.length; i++) {
    var positive = element.attributes[i];
    var asp = attributesDesc[positive];
    $desc = $('<div class="description display-flex"></div>')
    $finalProduct.append($desc);
    $desc.append(getTick('greentick') + asp);
  };
  // function plusMinusFilter(e) {
  //   if (e !== element.attributes[0] && e !== element.attributes[1] && e !== element.attributes[2] && e !== element.attributes[3] && e !== element.minuses[0] && e !== element.minuses[1] && e !== element.minuses[2]) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
  // var noPluses = Object.keys(attributesDesc).filter(plusMinusFilter);
  // for (var j = 0; j < 4 - element.attributes.length; j++) {
  //   var name = noPluses[j];
  //   var asp = attributesDesc[name];
  //   $desc = $('<div class="description display-flex"></div>')
  //   $finalProduct.append($desc);
  //   $desc.append(getTick('graytick') + asp);
  //   var index = noPluses.indexOf(name);
  //   if (index !== -1) noPluses.splice(index, 1);
  // }
  // for (var k = 0; k < noPluses.length; k++) {
  //   var name = noPluses[k];
  //   var asp = attributesDesc[name];
  //   $desc = $('<div class="description2 display-flex"></div>')
  //   $finalProduct.append($desc);
  //   $desc.append(getTick('graytick') + asp);
  // }
  // SHOW MINUSES
  /* for (var l = 0; l < element.minuses.length; l++) {
    var asp = attributesDesc[element.minuses[l]];
    $desc = $('<div class="description2 display-none"></div>')
    $finalProduct.append($desc);
    $desc.append(getTick('minus') + asp);
  } */
}

/* var aspects = function(element) {
  const aspects = element.description.filter(function(e) {return e.val !== 'minus'})
  for (var i = 0; i < 4 ; i++) {
    var value = getTick(aspects[i].val);
    var asp = '<p class="product-aspect">' + aspects[i].aspect + '</p>';
    $desc = $('<div class="description display-flex"></div>')
    $finalProduct.append($desc);
    $desc.append(value, asp);
  };
  for (var j = 4; j < aspects.length ; j++) {
    var value = getTick(aspects[i].val);
    var asp = '<p class="product-aspect">' + aspects[j].aspect + '</p>';
    $desc = $('<div class="description2 display-none"></div>')
    $finalProduct.append($desc);
    $desc.append(value, asp)
  }
} */

$( document ).on('click', ".show-more", (function() {
  $( this ).parent().children('.description2').removeClass('display-none');
  $( this ).parent().children('.description2').addClass('display-flex');
  $( this ).replaceWith(showLess);
}));

$( document ).on('click', ".show-less", (function() {
  $( this ).parent().children('.description2').addClass('display-none');
  $( this ).parent().children('.description2').removeClass('display-flex');
  $( this ).replaceWith(showMore);
}));

var saveAnswers = function() {
  $.post(window.quizServer + 'save.php', {
    data: JSON.stringify(answers),
    quiz: window.quizName
  }, function(rs){
    // console.log(rs)
  })
}

$( document ).on('click', ".nextbtn", (function() {
  $( ".step" ).empty();
  $(".step").append('<div class="question">Based on your nail type and goals, we recommend these products</div>');
  var selected = result()
  buildProduct(selected[0]);
  answers.product_1 = selected[0].name;
  buildProduct(selected[1]);
  answers.product_2 = selected[1].name;
  buildProduct(selected[2]);
  answers.product_3 = selected[2].name;
  saveAnswers()
  $(".step").append(quizAgain());
}))




// TAKE QUIZ AGAIN
$( document ).on('click', ".quizagain", (function() {
  for (var att in attributes) {
    attributes[att] = 0;
  }
  resetAttributes()
  currentStep = 0;
  showQuestion(currentStep);
}))


// RESULTS CALC
function result() {
  console.log(attributes)
  for (var i = 0; products.length > i; i++) {
    var product = products[i];
    for (var j = 0; product.attributes.length > j; j++) {
      var attribute = product.attributes[j];
      product.score += attributes[attribute];
    }
  }

  products.sort(function(a, b){
      return b.score-a.score
  })
  console.log(products)
  return products.slice(0, 3);
}

$('#quiz-aux-button').on('click', function(e){
  saveAnswers()
})