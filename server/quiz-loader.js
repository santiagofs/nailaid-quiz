(function(window) {
  var head  = document.getElementsByTagName('head')[0];
  var body = document.getElementsByTagName('body')[0];

  var jq = document.createElement('script');
  jq.type = "text/javascript";
  jq.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js';
  head.appendChild(jq);



  window.addEventListener('load', function (e)  {
    var $loader = $('#nail-aid-quiz-script');

    var urlArr = $loader.attr('src').split("/");
    var server = urlArr[0] + "//" + urlArr[2] + '/';
    window.quizServer = server;

   //var server = $loader.attr('remote') + '/';

    var st = document.createElement('link');
    st.href = server + 'stylesheet.css';
    st.type = 'text/css';
    st.rel = 'stylesheet';
    st.media = 'screen,print';
    head.appendChild(st);

    var $container = $('<div class="quiz-content"></div>');
    $loader.before($container)
    window.quizName = $loader.attr('quiz');
    $.getScript(server + 'products.js', function() {
      $.getScript(server + window.quizName + '.js', function() {
        $.getScript(server + 'logic.js');
      })
    })

  }, false);

})(window);


