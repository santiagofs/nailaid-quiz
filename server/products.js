var allProducts = [{
  id: 1,
  name : 'Keratin 3 Day Growth',
  attributes : ['baseCoat', 'strengthener', 'growth'],
  minuses : ['thickener', 'topCoat'],
  url : 'https://nailaidworks.com/products/keratin-3-day-growth',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/KERATIN_033cf1cd-e940-4e10-98dc-a19df18c112d_grande.png?v=1548182011',
  score: 0
}, {
  id: 2,
  name : 'Collagen Power Coat',
  attributes : ['baseCoat', 'strengthener', 'ridgeFiller'],
  minuses : ['thickener', 'topCoat'],
  url : 'https://nailaidworks.com/products/collagen-power-coat',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/COLLAGEN_3f2a65ee-9e50-4e56-9ce9-c98fd4d385e2_grande.png?v=1548181976',
  score: 0
}, {
  id: 3,
  name : 'Steel Instant Hardener',
  attributes : ['baseCoat', 'strengthener'],
  minuses : ['thickener', 'topCoat'],
  url : 'https://nailaidworks.com/products/steel-instant-hardener',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/STEEL_0f54e2ab-f9c9-4425-8f52-a598658012f1_grande.png?v=1548182114',
  score: 0
}, {
  id: 4,
  name : '1 Minute Artificials',
  attributes : ['topCoat', 'baseCoat', 'thickener', 'shineGloss'],
  minuses : [],
  url : 'https://nailaidworks.com/products/1-minute-gel-coat',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/1MINUTE_grande.png?v=1548181916',
  score: 0
}, {
  id: 5,
  name : 'Ceramide Extreme Thickener',
  attributes : ['baseCoat', 'thickener', 'ridgeFiller'],
  minuses : ['topCoat'],
  url : 'https://nailaidworks.com/products/ceramide-extreme-thickener',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/CERAMIDE_da18df3b-40f4-4734-b2c6-f9e9e32a6422_grande.png?v=1548181970',
  score: 0
}, {
  id: 6,
  name : 'Biotin Ultimate Strength',
  attributes : ['baseCoat', 'strengthener', 'growth'],
  minuses : ['thickener', 'topCoat'],
  url : 'https://nailaidworks.com/products/biotin-ultimate-strength',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/BIOTIN_grande.png?v=1548181965',
  score: 0
}, {
  id: 7,
  name : '8-in-1 Nude Manicure',
  attributes : ['topCoat', 'ridgeFiller', 'multiBenefit'],
  minuses : ['strengthener', 'thickener'],
  url : 'https://nailaidworks.com/products/8-in-1-nude-manicure',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/8IN1_a964232e-a952-4033-b95d-1049b3c4d049_grande.png?v=1548181938',
  score: 0
}, {
  id: 8,
  name : 'Sapphire Strength Mega Growth',
  attributes : ['baseCoat', 'strengthener', 'growth', 'shineGloss'],
  minuses : ['thickener', 'topCoat'],
  url : 'https://nailaidworks.com/products/sapphire-strength-mega-growth',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/SAPPHIRE_grande.png?v=1548182099',
  score: 0
}, {
  id: 9,
  name : 'Diamond Hard Overnight Strengthener Oil',
  attributes : ['strengthener', 'nailOil'],
  minuses : ['thickener', 'topCoat'],
  url : 'https://nailaidworks.com/products/diamond-hard-overnight-strengthener-oil',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/DIAMOND_5f90a632-8d91-4a3c-8627-63e108341d19_grande.png?v=1548181988',
  score: 0
}, {
  id: 10,
  name : 'After Artificials Rehab',
  attributes : ['repair'],
  minuses : ['strengthener', 'thickener', 'topCoat'],
  url : 'https://nailaidworks.com/products/copy-of-1-minute-artificials',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/AFTER-ARTIFICIALS_grande.png?v=1549645541',
  score: 0
}, {
  id: 11,
  name : 'Calcium Extra Strength Builder',
  attributes : ['baseCoat', 'strengthener', 'thickener', 'repair'],
  minuses : ['topCoat'],
  url : 'https://nailaidworks.com/products/calcium-extra-strength-builder',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/CALCIUM_grande.png?v=1549648363',
  score: 0
}, {
  id: 12,
  name : 'Total Cure 9-in-1',
  attributes : ['strengthener', 'multiBenefit'],
  minuses : ['thickener', 'topCoat'],
  url : 'https://nailaidworks.com/products/total-cure-9-in-1',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/9-IN-1_grande.png?v=1549647854',
  score: 0
}, {
  id: 13,
  name : 'Dip-Free Acrylics',
  attributes : ['strengthener', 'thickener'],
  minuses : ['topCoat'],
  url : 'https://nailaidworks.com/products/dip-free-acrylics',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/DIP-FREE_grande.png?v=1549649019',
  score: 0
}, {
  id: 14,
  name : 'Acrylic + Gel In One',
  attributes : ['baseCoat', 'strengthener', 'thickener', 'shineGloss'],
  minuses : ['topCoat'],
  url : 'https://nailaidworks.com/products/acrylic-gel-in-one',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/ACRYLIC-GEL_grande.png?v=1549649313',
  score: 0
}, {
  id: 15,
  name : 'Gel Xtreme Shine Top Coat',
  attributes : ['topCoat', 'shineGloss'],
  minuses : ['strengthener', 'thickener'],
  url : 'https://nailaidworks.com/products/gel-xtreme-shine-top-coat',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/GELXTREME_grande.png?v=1548182000',
  score: 0
}, {
  id: 16,
  name : 'Just Like Gel Top Coat',
  attributes : ['topCoat', 'strengthener', 'ridgeFiller', 'shineGloss'],
  minuses : ['thickener'],
  url : 'https://nailaidworks.com/products/just-like-gel-top-coat',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/JUSTLIKEGEL_grande.png?v=1548182006',
  score: 0
}, {
  id: 17,
  name : 'No Light Gel Top Coat',
  attributes : ['topCoat', 'shineGloss'],
  minuses : ['strengthener', 'thickener'],
  url : 'https://nailaidworks.com/products/no-light-gel-top-coat',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/NOLIGHT_grande.png?v=1548182044',
  score: 0
}, {
  id: 18,
  name : '3-in-1 Gel Base + Top Coat + Hardener',
  attributes : ['topCoat', 'baseCoat', 'multiBenefit', 'shineGloss'],
  minuses : ['strengthener', 'thickener', 'multiBenefit'],
  url : 'https://nailaidworks.com/products/3-in-1-gel-base-top-coat-hardener',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/3IN1_4c244128-6452-422f-b6c1-ab249626373b_grande.png?v=1548181932',
  score: 0
}, {
  id: 19,
  name : 'Pure Crystal Shine Top Coat',
  attributes : ['topCoat', 'shineGloss'],
  minuses : ['thickener'],
  url : 'https://nailaidworks.com/products/pure-crystal-shine-top-coat',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/PURE-CRYSTAL_grande.png?v=1549648741',
  score: 0
}, {
  id: 20,
  name : 'Iron Shield Top Coat',
  attributes : ['topCoat', 'strengthener', 'ridgeFiller', 'shineGloss'],
  minuses : ['thickener'],
  url : 'https://nailaidworks.com/products/iron-shield-top-coat',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/IRON-SHIELD_grande.png?v=1549649432',
  score: 0
}, {
  id: 21,
  name : 'Platinum Hi-Gloss Top Coat',
  attributes : ['topCoat', 'ridgeFiller', 'shineGloss'],
  minuses : ['thickener'],
  url : 'https://nailaidworks.com/products/platinum-hi-gloss-top-coat',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/PLATINUM_grande.png?v=1549649605',
  score: 0
}, {
  id: 22,
  name : 'Fiberglass + Gel Top Coat',
  attributes : ['topCoat', 'strengthener', 'shineGloss'],
  minuses : ['thickener'],
  url : 'https://nailaidworks.com/products/fiberglass-gel-top-coat',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/FIBERGLASS_grande.png?v=1549649181',
  score: 0
}, {
  id: 23,
  name : '1 Step French Mani',
  attributes : ['ridgeFiller', 'multiBenefit', 'shineGloss'],
  minuses : ['strengthener', 'thickener', 'topCoat'],
  url : 'https://nailaidworks.com/products/1-step-french-mani',
  img : 'https://cdn.shopify.com/s/files/1/1476/4490/products/1STEP_grande.png?v=1548181923',
  score: 0
}]

console.log('products loaded')